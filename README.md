# Physics-informed LSTM model (PILSTM) for Streamflow Prediction

This repository contains the code developed for the paper titled "A physics-informed machine learning model for streamflow prediction." The study compares the performance of LSTM and PILSTM models using data from 531 CAMELS-US catchments (Addor et al., 2017; A. J. Newman et al., 2015) and 8 CHOSEN catchments (Zhang et al., 2021). The code is derived from the NeuralHydrology code base (Kratzert et al., 2022), with modifications made to conduct the experiments outlined in our study.

## Models and Experiments

We evaluate the performance of five different models:

1. Stand-alone LSTM model with precipitation (PPT) and air temperature (AT) as inputs.
2. Stand-alone LSTM model with PPT and potential evapotranspiration (PET) as inputs.
3. PILSTM model with PPT and AT as inputs.
4. PILSTM model with PPT and PET as inputs.
5. Stand-alone physical model with PPT and PET as inputs.

We conduct experiments to assess model performance under different scenarios, including the optimal weighting of physical information, varying climate variability, and situations with limited training data. The reported results are averages obtained from 10 ensemble runs on the testing dataset.

## Description of the Repository

- **chosen_data**: Stores the CHOSEN dataset used in this study.
  
- **experiments**: Contains folders for each model, where **site_list** includes text files for dataset configurations, and **configs** stores configuration files for base experiments. Additional experiments (data-scarce and climate change) modify these configuration files, executed through the main running files (e.g., `LSTM_AT_PPT_climatechange_ensemble_drycreek.py`).

- **main**: Holds the main running files for the experiments.

- **neuralhydrology-pilstm**: A modified NeuralHydrology code base with specific modifications detailed below.

- **notebooks**: Jupyter notebooks for result analysis.

## Modifications to the NeuralHydrology Package

The following modifications were made within the `neuralhydrology` package:

1. **datasetzoo**: Added `chosen.py` to read and use data from the CHOSEN dataset. Modified `camelsus.py` to generate ET data using `get_priestley_taylor_pet` from the `datautils` subfolder.

2. **datautils**: Added `QS_physical_model.py` to build a physical model (Kirchner, 2009) based on storage-discharge dynamics.

3. **modelzoo**: Added `pilstm.py` to run the PILSTM model. Modified `cudalstm.py` to apply integrated gradients analysis.

4. **utils**: Modified `config.py` to include additional configuration arguments for the experiments in this study.

## Run the Code and Reproduce the Results

To reproduce the results, follow these steps:

1. **Non-pretraining cases (site-specific models):**
   - To train and evaluate the model for the DryCreek watershed in the base experiment, run:
     ```
     python experiments/LSTM_AT_PPT/main/base_nopretrain/LSTM_AT_PPT_base_nopretrain_drycreek.py
     ```
   - For data-scarce and climate change experiments, change the `base_experiment` folder to `data_scarce` or `climate_change`.

2. **Pretraining cases:**
   - To pretrain the model, run:
     ```
     nh-run train --experiments/LSTM_AT_PPT/configs/base_pretrain/531_basins_lstm_at_ppt_pretrain.yml
     ```
   - To finetune and evaluate the model for the DryCreek watershed in the base experiment, run:
     ```
     python experiments/LSTM_AT_PPT/main/base_pretrain/LSTM_AT_PPT_base_pretrain_drycreek.py
     ```
   - For data-scarce and climate change experiments, change the `base_experiment` folder to `data_scarce` or `climate_change`.