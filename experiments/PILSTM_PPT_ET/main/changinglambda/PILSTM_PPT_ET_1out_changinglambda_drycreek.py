import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import seaborn as sns
import pickle
import yaml
import copy
from pathlib import Path

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from neuralhydrology.evaluation.metrics import *
from neuralhydrology.nh_run import start_run, eval_run, finetune
import re
import xarray as xr
import shutil
import random


def calculate_avg_water_year_deficit(eval_pickle_file_path,
                                     QS_par_file_path,
                                     data_file_path):
    
    # check if it is water year
    with open(eval_pickle_file_path, 'rb') as file:
        eval_data = pickle.load(file)
    
    watershed_name = list(eval_data.keys())[0]
    test_dates = np.datetime_as_string(eval_data[watershed_name]['1D']['xr']['date'].values, unit='D')
    
    if test_dates[0].split('-')[1]=='10':
        is_water_year = True
    else:
        is_water_year = False
    print(f'is_water_year for {watershed_name} is', is_water_year)
    
    # get Q_pred
    q_test_pred = eval_data[watershed_name]['1D']['xr']['Qpilstm(mm/d)_sim'].values.reshape(-1)
    q_test_pred = pd.Series(q_test_pred)
    
    # get kp and ke par
    par_df = pd.read_csv(QS_par_file_path, index_col=0) 
    kP, kE = par_df.head(1)['kP'].values[0], par_df.head(1)['kE'].values[0]
    print('kP, kE are', kP, kE)
    
    # get PPT and PET arrays
    chosen_data = pd.read_csv(data_file_path, index_col='DateTime', parse_dates = True)
    chosen_data_test = chosen_data.loc[test_dates, :]
    
    ppt_col, pet_col = [], []
    for col in chosen_data_test.columns:
        if 'precipitation' in col.lower():
            ppt_col.append(col)
        elif 'evapo' in col.lower():
            pet_col.append(col)
    assert(len(ppt_col) == 1)
    assert(len(pet_col) == 1)
    
    print(ppt_col, pet_col)
    ppt_test = chosen_data_test[ppt_col[0]]
    pet_test = chosen_data_test[pet_col[0]]
    
    # calculate water deficit
    indexes = []
    if is_water_year:
        for n in range(1,len(test_dates)//365+1):
            indexes.append(365*n-1)
    else:
        for n in range(1,len(test_dates)//365):
            indexes.append(365*n-1+273)
    print('indexes are', indexes)

    wb_deficit_lst, wb_deficit_relative_lst = [], []
    
    for index in indexes:
        wb_deficit = kP*ppt_test.rolling(365).sum()[index] - kE*pet_test.rolling(365).sum()[index] - q_test_pred.rolling(365).sum()[index]
        ppt_sum = kP*ppt_test.rolling(365).sum()[index]
        
        # relative wb_deficit to water input mass
        wb_deficit_relative = np.abs(wb_deficit)/ppt_sum
        wb_deficit_lst.append(wb_deficit)
        wb_deficit_relative_lst.append(wb_deficit_relative)
    
    print('wb_deficit_lst is', wb_deficit_lst)
    print('wb_deficit_relative_lst is', wb_deficit_relative_lst)
    
    wb_deficit_relative_avg = np.mean(wb_deficit_relative_lst)
    
    return wb_deficit_relative_avg


def format_number_with_zeros(number):
    # Use string formatting to add leading zeros
    formatted_number = f"{number:03d}"
    return formatted_number


def get_validation_loss(file = "",
                         exp = "LSTM_AT_PPT",
                         epoch_n = 30,
                         col_name = 'Qpilstm(mm/d)_NSE'):
    metric_lst = []
    
    for i in range(1,epoch_n+1):
        formatted_number = format_number_with_zeros(i)
        path = f"{file}/validation/model_epoch{formatted_number}/validation_metrics.csv"
        valid_result_df = pd.read_csv(path)
        
        metric_value_median = valid_result_df[col_name].median()
        metric_lst.append(metric_value_median)
    
    return metric_lst


def create_folder(folder_path):
    if not os.path.exists(folder_path):
        try:
            os.makedirs(folder_path)
            print(f"Folder '{folder_path}' created successfully.")
        except OSError as e:
            print(f"Error creating folder '{folder_path}': {e}")
    else:
        print(f"Folder '{folder_path}' already exists.")


exp = "PILSTM_PPT_ET_1out"
exp_lower = exp.lower()

scenario = 'changinglambda'

watersheds = ['drycreek',
 'hjandrews',
 'harvardforest',
 'hubbardbrook',
 'jornadabasin',
 'kellogg',
 'konzaprairie',
 'sevilleta']

valid_res = []
eval_res = []

random.seed(0)
random_integers = [random.randint(1, 1000) for _ in range(10)]
print(random_integers)

watershed = 'drycreek' ###########
scenario = 'changinglambda' # run ensembles for the base exp

for lambda_value in np.linspace(0, 1, num=101): 
    print(lambda_value)

    for random_number in random_integers:
        print(random_number)

        original_yml_file_path = f'configs/high_lr/finetune_{watershed}_{exp_lower}_high_lr.yml'
        with open(original_yml_file_path, 'r') as original_file:
            config_data_original = yaml.safe_load(original_file)

        config_data = copy.deepcopy(config_data_original)
        config_data['scenario'] = 'ensemble'
        config_data['lambda_pilstm'] = float(lambda_value)
        config_data['seed'] = random_number

        former_experiment_name = config_data['experiment_name']
        config_data['experiment_name'] = f'{former_experiment_name}_{scenario}_{round(lambda_value,2)}lambda_{random_number}seed'
        
        create_folder(f'configs/{scenario}')
        yml_file_path = f'configs/{scenario}/finetune_{watershed}_{exp_lower}_{scenario}_{round(lambda_value,2)}lambda_{random_number}seed.yml'
        
        with open(yml_file_path, 'w') as file:
            yaml.dump(config_data, file, default_flow_style=False)

        finetune(Path(yml_file_path)) ###################################################
        os.remove(yml_file_path)

        # get the best epoch for finetuning results
        files = glob.glob(f"runs/*_finetune_{watershed}_{scenario}_{round(lambda_value,2)}lambda_{random_number}seed_*")
        print(files)
        assert(len(files)==1)

        file = files[0]
        folder = file.rsplit('/')[-1]

        max_nse, optimal_epoch = -10000, None
        for epoch_number in range(2,31): ##### force optimal epoch >1

            formatted_number = format_number_with_zeros(epoch_number)
            valid_result_pickle_file_per_epoch = f'{file}/validation/model_epoch{formatted_number}/validation_results.p'
            
            with open(valid_result_pickle_file_per_epoch, "rb") as fp:
                results = pickle.load(fp)
                
            watershed_name = list(results.keys())[0]
            valid_dates = results[watershed_name]['1D']['xr']['date'].values

            data_dir = config_data['data_dir']
            data_file_path = f'{data_dir}/{watershed_name}_12yrs_ET.csv'
            chosen_data = pd.read_csv(data_file_path, index_col='DateTime', parse_dates = True)
            
            for Q_col in chosen_data.columns:
                if 'discharge' in Q_col.lower():
                    #print('Discharge column is', Q_col)
                    break

            QObs = xr.DataArray(chosen_data.loc[valid_dates,Q_col].values)
            QPred = xr.DataArray(results[watershed_name]['1D']['xr']['Qpilstm(mm/d)_sim'].values.reshape(-1))

            if nse(QObs, QPred) >= max_nse:
                max_nse = nse(QObs, QPred)
                optimal_epoch = epoch_number

            valid_res.append({'watershed': watershed,
                        'seed': int(random_number),
                        'lambda': lambda_value,
                        'epoch': epoch_number,
                        'NSE': nse(QObs, QPred),
                        'MSE': mse(QObs, QPred),
                        'FHV': fdc_fhv(QObs, QPred),
                        'FMS': fdc_fms(QObs, QPred),
                        'FLV': fdc_flv(QObs, QPred),
                        'Pearson-r': pearsonr(QObs, QPred),
                        'KGE': kge(QObs, QPred),
                        'Beta-KGE': beta_kge(QObs, QPred)
                        }) 

        # start evaluation for the optimal epoch
        run_dir = f'/global/home/users/angelikazhang/Research/neuralhydrology-master/base_experiment/{exp}/runs/{folder}'
        eval_run(run_dir=Path(run_dir), epoch = optimal_epoch, period="test")

        eval_files = glob.glob(f"runs/{exp_lower}_finetune_{watershed}_{scenario}_{round(lambda_value,2)}lambda_{random_number}seed_*/test/model_epoch*/test_metrics.csv")
        assert(len(eval_files)==1)
        metrics_df = pd.read_csv(eval_files[0])

        eval_pickle_file_path = eval_files[0].rsplit('/',1)[0] + '/test_results.p'
        QS_par_file_paths = glob.glob(f'/global/home/users/angelikazhang/Research/neuralhydrology-master/base_experiment/calibration_results/baseexperiment/{watershed_name}_QSmodel_parameters_searching_*')
        
        assert(len(QS_par_file_paths)==1)
        QS_par_file_path = QS_par_file_paths[0]
         
        metrics_df = pd.read_csv(eval_files[0])
        wb_deficit_avg = calculate_avg_water_year_deficit(eval_pickle_file_path, QS_par_file_path, data_file_path)

        res_d = {'watershed': watershed,
                 'seed': int(random_number),
                 'lambda': lambda_value,
                 'optimal_epoch': optimal_epoch,
                 'wb_deficit_avg':wb_deficit_avg}
            
        for metric in metrics_df.columns: # 'basin' is also one of the columns
            res_d[metric] = metrics_df[metric].values[0]
        eval_res.append(res_d)

        # store eval results
        eval_df = pd.DataFrame(eval_res)
        eval_df.sort_values(by=['watershed','lambda','seed']).to_csv(f'{exp}_{scenario}_eval_result_{watershed}.csv')

        # Check if the file exists before attempting to remove it
        directory_path = run_dir  # Replace with the path to your directory

        # Check if the directory exists before attempting to remove it
        if os.path.exists(directory_path) and os.path.isdir(directory_path):
            shutil.rmtree(directory_path, ignore_errors=True)

# store all the validation records
validation_df = pd.DataFrame(valid_res)
validation_df.sort_values(by=['watershed','lambda','seed']).to_csv(f'{exp}_{scenario}_finetune_valid_result_{watershed}.csv')

# store optimal epochs 
optimal_epoch_finetune_valid_df = validation_df.loc[validation_df.groupby(['watershed', 'lambda', 'seed'])['NSE'].idxmax()]
optimal_epoch_finetune_valid_df.sort_values(by=['watershed','lambda','seed']).to_csv(f'{exp}_{scenario}_finetune_valid_result_optimal_epoch_{watershed}.csv')
