#!/bin/bash

#SBATCH --job-name=run_lstm
#
#SBATCH --account=fc_esdl
#
# Partition:
#SBATCH --partition=savio2_gpu
#
# Number of nodes:
#SBATCH --nodes=1
#
# Number of tasks (one for each GPU desired for use case) (example):
#SBATCH --ntasks=1
#
# Processors per task:
# Always at least twice the number of GPUs (savio2_gpu and GTX2080TI in savio3_gpu)
# Four times the number for TITAN and V100 in savio3_gpu
# Eight times the number for A40 in savio3_gpu
#SBATCH --cpus-per-task=4
#
#Number of GPUs, this can be in the format of "gpu:[1-4]", or "gpu:K80:[1-4] with the type included
#SBATCH --gres=gpu:1
#
# Wall clock limit:
#SBATCH --time=48:00:00
#
#SBATCH --output="./LSTM_ET_PPT_base_nopretrain_konzaprairie_%j.out"
#
#SBATCH --mail-type=END,FAIL
#
#SBATCH --mail-user=angelikazhang@berkeley.edu

# Commands to run:
python LSTM_ET_PPT_base_nopretrain_konzaprairie.py

echo "script execution complete"