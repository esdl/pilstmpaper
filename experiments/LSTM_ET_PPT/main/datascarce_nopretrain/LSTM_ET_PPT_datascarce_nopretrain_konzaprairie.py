import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import seaborn as sns
import pickle
import yaml
import copy
from pathlib import Path

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from neuralhydrology.evaluation.metrics import *
from neuralhydrology.nh_run import start_run, eval_run, finetune
import re
import xarray as xr
import shutil
import random


def calculate_avg_water_year_deficit(eval_pickle_file_path,
                                     QS_par_file_path,
                                     data_file_path,
                                     pred_col_name='Qpilstm(mm/d)_sim'):
    
    # check if it is water year
    with open(eval_pickle_file_path, 'rb') as file:
        eval_data = pickle.load(file)
    
    watershed_name = list(eval_data.keys())[0]
    test_dates = np.datetime_as_string(eval_data[watershed_name]['1D']['xr']['date'].values, unit='D')
    
    if test_dates[0].split('-')[1]=='10':
        is_water_year = True
    else:
        is_water_year = False
    print(f'is_water_year for {watershed_name} is', is_water_year)
    
    # get Q_pred
    q_test_pred = eval_data[watershed_name]['1D']['xr'][pred_col_name].values.reshape(-1)
    q_test_pred = pd.Series(q_test_pred)
    
    # get kp and ke par
    par_df = pd.read_csv(QS_par_file_path, index_col=0) 
    kP, kE = par_df.head(1)['kP'].values[0], par_df.head(1)['kE'].values[0]
    print('kP, kE are', kP, kE)
    
    # get PPT and PET arrays
    chosen_data = pd.read_csv(data_file_path, index_col='DateTime', parse_dates = True)
    chosen_data_test = chosen_data.loc[test_dates, :]
    
    ppt_col, pet_col = [], []
    for col in chosen_data_test.columns:
        if 'precipitation' in col.lower():
            ppt_col.append(col)
        elif 'evapo' in col.lower():
            pet_col.append(col)
    assert(len(ppt_col) == 1)
    assert(len(pet_col) == 1)
    
    print(ppt_col, pet_col)
    ppt_test = chosen_data_test[ppt_col[0]]
    pet_test = chosen_data_test[pet_col[0]]
    
    # calculate water deficit
    indexes = []
    if is_water_year:
        for n in range(1,len(test_dates)//365+1):
            indexes.append(365*n-1)
    else:
        for n in range(1,len(test_dates)//365):
            indexes.append(365*n-1+273)
    print('indexes are', indexes)

    wb_deficit_lst, wb_deficit_relative_lst = [], []
    
    for index in indexes:
        wb_deficit = kP*ppt_test.rolling(365).sum()[index] - kE*pet_test.rolling(365).sum()[index] - q_test_pred.rolling(365).sum()[index]
        ppt_sum = kP*ppt_test.rolling(365).sum()[index]
        
        # relative wb_deficit to water input mass
        wb_deficit_relative = np.abs(wb_deficit)/ppt_sum
        wb_deficit_lst.append(wb_deficit)
        wb_deficit_relative_lst.append(wb_deficit_relative)
    
    print('wb_deficit_lst is', wb_deficit_lst)
    print('wb_deficit_relative_lst is', wb_deficit_relative_lst)
    
    wb_deficit_relative_avg = np.mean(wb_deficit_relative_lst)
    
    return wb_deficit_relative_avg


def format_number_with_zeros(number):
    # Use string formatting to add leading zeros
    formatted_number = f"{number:03d}"
    return formatted_number


def get_validation_loss(file = "",
                         exp = "LSTM_AT_PPT",
                         epoch_n = 30,
                         col_name = 'Qpilstm(mm/d)_NSE'):
    metric_lst = []
    
    for i in range(1,epoch_n+1):
        formatted_number = format_number_with_zeros(i)
        path = f"{file}/validation/model_epoch{formatted_number}/validation_metrics.csv"
        valid_result_df = pd.read_csv(path)
        
        metric_value_median = valid_result_df[col_name].median()
        metric_lst.append(metric_value_median)
    
    return metric_lst


def create_folder(folder_path):
    if not os.path.exists(folder_path):
        try:
            os.makedirs(folder_path)
            print(f"Folder '{folder_path}' created successfully.")
        except OSError as e:
            print(f"Error creating folder '{folder_path}': {e}")
    else:
        print(f"Folder '{folder_path}' already exists.")


base_dir = '/global/home/users/angelikazhang/Research/neuralhydrology-master/base_experiment'

exp = "LSTM_ET_PPT" ###########
if 'PILSTM' in exp:
    pred_col_name = 'Qpilstm(mm/d)_sim'
else:
    pred_col_name = 'QObs(mm/d)_sim'

watershed = 'konzaprairie' ###########
scenario = 'datascarce_nopretrain' ###########
lambda_value_num = 1 # set for but input None later

random.seed(0)
random_integers = [random.randint(1, 1000) for _ in range(10)]  
print(random_integers)

exp_lower = exp.lower()

valid_res = []
eval_res = []

for training_years in range(1,7):
    print(training_years)
    reduce_yrs = 6-training_years

    # for each training_years, find the optimal epoch and lambda and do the evaluation
    for random_number in random_integers:
        print(random_number)

        for lambda_value in np.linspace(0, 1, num=lambda_value_num): #######
            print(lambda_value)

            # set up the climate change scenario configs from the original yaml file under this exp folder
            original_yml_file_path = f'configs/high_lr/finetune_{watershed}_{exp_lower}_high_lr.yml'
            with open(original_yml_file_path, 'r') as original_file:
                config_data_original = yaml.safe_load(original_file)

            config_data = copy.deepcopy(config_data_original)
            config_data['scenario'] = scenario
            #config_data['dry_wet_index'] = dry_wet_index
            config_data['lambda_pilstm'] = None
            config_data['seed'] = random_number

            # since pretraining is not needed, delete the base_run_dir
            config_data['base_run_dir'] = None
            config_data['checkpoint_path'] = None

            # set up new training and testing dates
            former_start_date_str = config_data['train_start_date']
            former_start_date = datetime.strptime(former_start_date_str, '%d/%m/%Y')
            
            new_train_start_date = former_start_date + relativedelta(years=reduce_yrs)
            config_data['train_start_date'] = new_train_start_date.strftime('%d/%m/%Y')

            train_start_date_lst = [config_data['train_start_date']]
            train_end_date_lst = [config_data['train_end_date']]

            #data_split_df = pd.read_csv(f'{base_dir}/data_split_df.csv', index_col=0)
            #record = data_split_df[(data_split_df['watershed']==watershed)&(data_split_df['dry_wet_index_new']==dry_wet_index)] ##### new_index combines train and validation data

            # extract training dates as lists
            #pattern = r'\d{2}/\d{2}/\d{4}' 
            #train_start_date_lst = re.findall(pattern, record['train_start_dates'].values[0])
            #train_end_date_lst = re.findall(pattern, record['train_end_dates'].values[0])

            #config_data['train_start_date'] = train_start_date_lst
            #config_data['train_end_date'] = train_end_date_lst
            
            #config_data['validation_start_date'] = record['validation_start_date'].values[0]
            #config_data['validation_end_date'] = record['validation_end_date'].values[0]
            #config_data['test_start_date'] = record['test_start_date'].values[0]
            #config_data['test_end_date'] = record['test_end_date'].values[0]
            
            experiment_name = f'{exp_lower}_train_{watershed}_{scenario}_{training_years}_{random_number}seed_{round(lambda_value,2)}lambda'
            config_data['experiment_name'] = experiment_name
            
            create_folder(f'configs/{scenario}')
            yml_file_path = f'configs/{scenario}/{experiment_name}.yml'
            
            with open(yml_file_path, 'w') as file:
                yaml.dump(config_data, file, default_flow_style=False)

            start_run(Path(yml_file_path)) ###################################################
            os.remove(yml_file_path)

            # get the best epoch for training results
            run_folder = glob.glob(f"runs/{experiment_name}_*")
            print(run_folder)
            assert(len(run_folder)==1)
            run_folder = run_folder[0]

            max_nse, optimal_epoch = -10000, None
            for epoch_number in range(1,31): 

                formatted_number = format_number_with_zeros(epoch_number)
                valid_result_pickle_file_per_epoch = f'{run_folder}/validation/model_epoch{formatted_number}/validation_results.p'
                
                with open(valid_result_pickle_file_per_epoch, "rb") as fp:
                    results = pickle.load(fp)
                    
                watershed_name = list(results.keys())[0]
                valid_dates = results[watershed_name]['1D']['xr']['date'].values

                data_dir = config_data['data_dir']
                data_file_path = f'{data_dir}/{watershed_name}_12yrs_ET.csv'
                chosen_data = pd.read_csv(data_file_path, index_col='DateTime', parse_dates = True)
                
                for Q_col in chosen_data.columns:
                    if 'discharge' in Q_col.lower():
                        #print('Discharge column is', Q_col)
                        break

                QObs = xr.DataArray(chosen_data.loc[valid_dates,Q_col].values)
                QPred = xr.DataArray(results[watershed_name]['1D']['xr'][pred_col_name].values.reshape(-1))

                if nse(QObs, QPred) >= max_nse:
                    max_nse = nse(QObs, QPred)
                    optimal_epoch = epoch_number

                valid_res.append({
                            'exp': exp_lower,
                            'scenario': scenario,
                            'watershed': watershed,
                            'training_years': training_years,
                            'seed': int(random_number),

                            'lambda': lambda_value,
                            'epoch': epoch_number,

                            'NSE': nse(QObs, QPred),
                            'MSE': mse(QObs, QPred),
                            'FHV': fdc_fhv(QObs, QPred),
                            'FMS': fdc_fms(QObs, QPred),
                            'FLV': fdc_flv(QObs, QPred),
                            'Pearson-r': pearsonr(QObs, QPred),
                            'KGE': kge(QObs, QPred),
                            'Beta-KGE': beta_kge(QObs, QPred)
                            }) 

            # get the optimal epoch and for current dry-wet-index and random seed
            validation_df_current_index_seed_lambda = pd.DataFrame(valid_res)
            validation_df_current_index_seed_lambda = validation_df_current_index_seed_lambda[(validation_df_current_index_seed_lambda['training_years']==training_years) & (validation_df_current_index_seed_lambda['seed']==int(random_number))]
            validation_df_current_index_seed_lambda = validation_df_current_index_seed_lambda[validation_df_current_index_seed_lambda['lambda']==lambda_value]
            
            assert(len(validation_df_current_index_seed_lambda)==30) # sanity check

            optimal_epoch = validation_df_current_index_seed_lambda.loc[validation_df_current_index_seed_lambda['NSE'].idxmax()]['epoch'] 

            # start evaluation for the optimal epoch
            eval_run(run_dir=Path(run_folder), epoch = optimal_epoch, period="test")

            # get eval files
            formatted_number_epoch = format_number_with_zeros(optimal_epoch)
            eval_file = f"{run_folder}/test/model_epoch{formatted_number_epoch}/test_metrics.csv"

            metrics_df = pd.read_csv(eval_file)
            eval_pickle_file_path = eval_file.rsplit('/',1)[0] + '/test_results.p'

            # get par file
            par_file_name = f'{watershed_name}_QSmodel_parameters_searching'
            
            for train_start_date, train_end_date in zip(train_start_date_lst, train_end_date_lst): # train_start_date is string
                
                train_start_date_str = datetime.strptime(train_start_date, '%d/%m/%Y').strftime('%Y-%m-%d')
                train_end_date_str = datetime.strptime(train_end_date, '%d/%m/%Y').strftime('%Y-%m-%d')
                
                par_file_name += f'_{train_start_date_str}_{train_end_date_str}'
            
            print('par_file_name is', par_file_name)

            QS_par_file_path = f'{base_dir}/calibration_results/{par_file_name}.csv'
            wb_deficit_avg = calculate_avg_water_year_deficit(eval_pickle_file_path, QS_par_file_path, data_file_path, pred_col_name)

            # record the optimal epoch and lambda
            eval_res_d = {
            'exp': exp_lower,
            'scenario': scenario,

            'watershed': watershed,
            'training_years': training_years,

            'seed': int(random_number),
            'lambda': lambda_value,
            
            'optimal_epoch': optimal_epoch,
            'wb_deficit_avg':wb_deficit_avg}
                
            for metric in metrics_df.columns: # 'basin' is also one of the columns
                eval_res_d[metric] = metrics_df[metric].values[0]
        
            eval_res.append(eval_res_d)

            # store validation and eval results every evluation run
            pd.DataFrame(valid_res).sort_values(by=['seed','lambda','epoch']).to_csv(f'{exp}_{scenario}_{watershed}_validation_result.csv')
            pd.DataFrame(eval_res).sort_values(by=['seed','lambda']).to_csv(f'{exp}_{scenario}_{watershed}_eval_result.csv')

            # remove the run folders to save space 
            directory_path = f'{base_dir}/{exp}/{run_folder}'

            print(directory_path)

            # Check if the directory exists before attempting to remove it
            if os.path.exists(directory_path) and os.path.isdir(directory_path):
                shutil.rmtree(directory_path, ignore_errors=True)
                print(f'successfully removed {directory_path}')
            else:
                print(f'{directory_path} is not found!')




