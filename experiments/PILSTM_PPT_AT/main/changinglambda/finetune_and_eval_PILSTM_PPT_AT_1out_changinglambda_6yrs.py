import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import seaborn as sns
import pickle
import yaml
import copy
from pathlib import Path

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from neuralhydrology.evaluation.metrics import *
from neuralhydrology.nh_run import start_run, eval_run, finetune
import re
import xarray as xr


def format_number_with_zeros(number):
    # Use string formatting to add leading zeros
    formatted_number = f"{number:03d}"
    return formatted_number


def get_validation_loss(file = "",
                         exp = "LSTM_AT_PPT",
                         epoch_n = 30,
                         col_name = 'Qpilstm(mm/d)_NSE'):
    metric_lst = []
    
    for i in range(1,epoch_n+1):
        formatted_number = format_number_with_zeros(i)
        path = f"{file}/validation/model_epoch{formatted_number}/validation_metrics.csv"
        valid_result_df = pd.read_csv(path)
        
        metric_value_median = valid_result_df[col_name].median()
        metric_lst.append(metric_value_median)
    
    return metric_lst


exp = "PILSTM_PPT_AT_1out"
exp_lower = exp.lower()

scenario = 'changinglambda'
training_years = 6

watersheds = ['drycreek',
 'hjandrews',
 'harvardforest',
 'hubbardbrook',
 'jornadabasin',
 'kellogg',
 'konzaprairie',
 'sevilleta']

valid_res = []
eval_res = []

for lambda_value in np.linspace(0, 1, num=101): #1):
    print(lambda_value)
    for watershed in watersheds:
        print(watershed)

        yml_file_path = f'configs/{scenario}/finetune_{watershed}_{exp_lower}_{scenario}_{training_years}yrs_{round(lambda_value,2)}lambda.yml'
        finetune(Path(yml_file_path))

        with open(yml_file_path, 'r') as yml_file:
            config_data = yaml.safe_load(yml_file)

        # get the best epoch for finetuning results
        files = glob.glob(f"runs/*_finetune_{watershed}_{scenario}_{training_years}yrs_{round(lambda_value,2)}lambda_*")
        assert(len(files)==1)

        file = files[0]
        folder = file.rsplit('/')[-1]

        max_nse, optimal_epoch = -10000, None
        for epoch_number in range(1,31):

            formatted_number = format_number_with_zeros(epoch_number)
            valid_result_pickle_file_per_epoch = f'{file}/validation/model_epoch{formatted_number}/validation_results.p'
            
            with open(valid_result_pickle_file_per_epoch, "rb") as fp:
                results = pickle.load(fp)
                
            watershed_name = list(results.keys())[0]
            valid_dates = results[watershed_name]['1D']['xr']['date'].values

            data_dir = config_data['data_dir']
            data_file_path = f'{data_dir}/{watershed_name}_12yrs_ET.csv'
            chosen_data = pd.read_csv(data_file_path, index_col='DateTime', parse_dates = True)
            
            for Q_col in chosen_data.columns:
                if 'discharge' in Q_col.lower():
                    #print('Discharge column is', Q_col)
                    break

            QObs = xr.DataArray(chosen_data.loc[valid_dates,Q_col].values)
            QPred = xr.DataArray(results[watershed_name]['1D']['xr']['Qpilstm(mm/d)_sim'].values.reshape(-1))

            if nse(QObs, QPred) >= max_nse:
                max_nse = nse(QObs, QPred)
                optimal_epoch = epoch_number

            valid_res.append({'watershed': watershed,
                        'training_years': int(training_years),
                        'lambda': lambda_value,
                        'epoch': epoch_number,
                        'NSE': nse(QObs, QPred),
                        'MSE': mse(QObs, QPred),
                        'FHV': fdc_fhv(QObs, QPred),
                        'FMS': fdc_fms(QObs, QPred),
                        'FLV': fdc_flv(QObs, QPred),
                        'Pearson-r': pearsonr(QObs, QPred),
                        'KGE': kge(QObs, QPred),
                        'Beta-KGE': beta_kge(QObs, QPred)
                        }) 

        # start evaluation for the optimal epoch
        run_dir = f'/global/home/users/angelikazhang/Research/neuralhydrology-master/base_experiment/{exp}/runs/{folder}'
        eval_run(run_dir=Path(run_dir), epoch = optimal_epoch, period="test")

        eval_files = glob.glob(f"runs/*_finetune_*_{scenario}_{training_years}yrs_*/test/model_epoch*/test_metrics.csv")
        assert(len(eval_files)==1)

        metrics_df = pd.read_csv(eval_files[0])
        res_d = {'watershed': watershed,
                 'training_years': int(training_years),
                 'lambda': lambda_value,
                 'optimal_epoch': optimal_epoch}
            
        for metric in metrics_df.columns: # 'basin' is also one of the columns
            res_d[metric] = metrics_df[metric].values[0]
        eval_res.append(res_d)

        # store eval results
        eval_df = pd.DataFrame(eval_res)
        eval_df.sort_values(by=['watershed','lambda','training_years']).to_csv(f'{exp}_{scenario}_eval_result_{training_years}yrs.csv')

        # Check if the file exists before attempting to remove it

        import shutil
        directory_path = run_dir  # Replace with the path to your directory

        # Check if the directory exists before attempting to remove it
        if os.path.exists(directory_path) and os.path.isdir(directory_path):
            shutil.rmtree(directory_path, ignore_errors=True)

# store all the validation records
validation_df = pd.DataFrame(valid_res)
validation_df.sort_values(by=['watershed','lambda','training_years']).to_csv(f'{exp}_{scenario}_finetune_valid_result_{training_years}yrs.csv')

# store optimal epochs 
optimal_epoch_finetune_valid_df = validation_df.loc[validation_df.groupby(['watershed', 'lambda', 'training_years'])['NSE'].idxmax()]
optimal_epoch_finetune_valid_df.sort_values(by=['watershed','lambda','training_years']).to_csv(f'{exp}_{scenario}_finetune_valid_result_optimal_epoch_lambda_{training_years}yrs.csv')
