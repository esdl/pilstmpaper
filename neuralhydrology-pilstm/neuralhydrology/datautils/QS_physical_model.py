###############
import torch
import copy
import pandas as pd
import numpy as np
import statsmodels.api as sm
import time
import itertools
import random
from scipy.stats import norm
from sklearn.gaussian_process import GaussianProcessRegressor
import os
from datetime import datetime, timedelta

if torch.cuda.is_available():
    dev = "cuda:0"
else:
    dev = "cpu"
    
device = torch.device(dev)


class QS_physical_model():
    """
    A simple single equation rainfall-runoff model developed by Kirchner, J. W based on discharge-storage relationship of a catchment.

    References
    ----------
    .. [#] Kirchner, J. W. (2009). Catchments as simple dynamical systems: Catchment characterization, rainfall-runoff modeling, 
    and doing hydrology backward. Water Resources Research, 45(2). https://doi.org/10.1029/2008WR006912
    """
    
    
    def __init__(self, 
                 basin,
                 catchment_data: pd.DataFrame,
                 train_start_dates: list = [], 
                 train_end_dates: list = [], 
                 timestep: int = 24,
                 save_path: str = '',
                 init_sample_n: int = 500,  
                 opt_iter_n: int = 15000, 
                 sample_n_acq: int = 5000,  
                 sample_n_score_calculated: int = 10
                 ):
        """
        Inputs
        -----------
        catchment_data: make sure 'PRCP(mm/day)', 'PET(mm/day)' and 'QObs(mm/d)' are columns in this dataframe
        train_index: np.array, 
        valid_index: np.array, 
        test_index: np.array, 
        timestep: integration timestep for daily data. For example, if the value is 24, delta_t is 1 hour.
        load_paramater: if True, skip calibrating and load from saved results

        Part 1
        - If do_baye_opt is false, read par file from save_path and set up the parameters
        - If do_baye_opt is true:
        1. get recession_df within train_index, pre-estimate c1, c2, c3 range
        2. write integrate_train function, which initialize model with the 1st Q of each training year and integrate all the following Q, 
        return the NSE loss (will use as objective function later)
        3. bayesian optimization and set up optimal parameters

        Part 2
        With the optimal parameters, integrate all the catchment_data, for consistency's sake, initialize with 1st Q of each training year
        
        """
        assert('PRCP(mm/day)' in catchment_data.columns)
        assert('PET(mm/day)' in catchment_data.columns)
        assert('QObs(mm/d)' in catchment_data.columns)

        self.basin = basin
        self.catchment_data = catchment_data 

        #train_start_dates = [datetime.strptime(train_start_date, '%d/%m/%Y') for train_start_date in train_start_dates]
        #train_end_dates = [datetime.strptime(train_end_date, '%d/%m/%Y') for train_end_date in train_end_dates]
        train_date_pairs = zip(train_start_dates, train_end_dates)
        self.train_index = np.array([start + timedelta(days=x) for start, end in train_date_pairs for x in range((end - start).days + 1)])
   
        self.save_path = save_path
        self.timestep = timestep 

        self.GPRmodel = GaussianProcessRegressor()  
        self.init_sample_n = init_sample_n
        self.opt_iter_n = opt_iter_n
        self.sample_n_acq = sample_n_acq
        self.sample_n_score_calculated = sample_n_score_calculated
    
        print('\nInitiate the physical model to prepare for the training of the PILSTM model')
        print('Calculate PEQ statistics')
        self.stat_d = self.get_PEQ_stat()
        
        print('Prepare the data for parameters calibration')
        self.train_df = self.prepare_train_df()
        self.Qmin = np.min(self.train_df['QObs(mm/d)'])
        self.Qmax = np.max(self.train_df['QObs(mm/d)'])

        print('Estimate parameter ranges')
        self.parameter_range = {}
        self.set_up_parameter_range() # set up for c1, c2 and c3
        self.parameter_range['runoff_coef'] = [0.1, 0.9]
        print('Initial parameter ranges are', self.parameter_range)

        self.par_file_name = f'{self.basin}_QSmodel_parameters_searching'
        for train_start_date, train_end_date in zip(train_start_dates, train_end_dates):
            train_start_date_str = train_start_date.strftime('%Y-%m-%d')
            train_end_date_str = train_end_date.strftime('%Y-%m-%d')
            self.par_file_name += f'_{train_start_date_str}_{train_end_date_str}'
        self.par_file_name += f'.csv'
        print('self.par_file_name is', self.par_file_name)
        
        self.t1 = time.time()
        if self.par_file_name in os.listdir(save_path):
            print('\nRead former calibration results')
            saved_par_df = pd.read_csv(save_path+'/'+self.par_file_name, index_col= 0)
            saved_par_df = saved_par_df.sort_values('NSE', ascending=False)
            
            self.res = saved_par_df.to_dict(orient='records')
            print(f'Read {len(self.res)} records')
            
            self.update_parameter_range()
            print('Current parameter range is', self.parameter_range)

            if self.is_converge():
                print('Saved results have converged. Set up the optimal parameters...')
                self.set_up_optimal_par()
            
            else:
                self.bayesian_optimization()
                self.set_up_optimal_par()

        else:
            print('No former calibration results')
            self.res = []
            self.bayesian_optimization()
            self.set_up_optimal_par()

        
        print('Done with QS model initialization.')


    def get_PEQ_stat(self):
        stat_d = {}
        stat_d['Q_mean'], stat_d['Q_std'] = self.catchment_data.mean()['QObs(mm/d)'], self.catchment_data.std()['QObs(mm/d)']
        stat_d['P_mean'], stat_d['P_std'] = self.catchment_data.mean()['PRCP(mm/day)'], self.catchment_data.std()['PRCP(mm/day)']
        stat_d['E_mean'], stat_d['E_std'] = self.catchment_data.mean()['PET(mm/day)'], self.catchment_data.std()['PET(mm/day)']
        return stat_d


    def prepare_train_df(self):
        """
        Return
        ------
        train_df: a dataframe with tarin_index having columns of P(PRCP(mm/day), E(PET(mm/day)), Q(QObs(mm/d)), dQ(mm/d), Qavg(mm/d), gQ, lnQavg
        
        dQ(t) = Q(t) - Q(t-1) , dQ(0) = 0
        Qavg(t) =  (Q(t) - Q(t-1))/2 , Qavg(0) = Q(0)
        gQ(t) = dQ(t)/Qavg(t)
        lnQavg(t) = np.log(Qavg(t))

        """
        train_df = copy.deepcopy(self.catchment_data.loc[:, ['PRCP(mm/day)', 'PET(mm/day)', 'QObs(mm/d)']])
        Qts = train_df['QObs(mm/d)'].values

        train_df['dQ(mm/d)'] = np.append(0, Qts[1:] - Qts[:-1])  
        train_df['Qavg(mm/d)'] = np.append(Qts[0], (Qts[1:]+Qts[:-1])/2) + 1e-8 # add eposilon to avoid zero division later
        train_df['gQ'] = -train_df['dQ(mm/d)']/train_df['Qavg(mm/d)']  
        train_df['lnQavg'] = np.log(train_df['Qavg(mm/d)'])

        train_df = train_df.loc[self.train_index, :]
        #print('print out the head of training df')
        #print(train_df.head()) 

        #print('print out the tail of training df')
        #print(train_df.tail()) 
        
        return train_df
    

    #def get_recession_df_former(self,df):
        """
        Recession data are data where P<<Q and E<<Q
        Here we start with P << 0.1*Q and PET << 0.1*Q
        If the recession data end up with none or only a small amount of data, we increase the threshold of 0.1
        Use at least 5% of the data as recesssion data

        """
        #for threshold in np.arange(0.01,0.5,0.01):
         #   bool1 = df['PRCP(mm/day)']<df['QObs(mm/d)'] * threshold
          #  bool2 = df['PET(mm/day)']<df['QObs(mm/d)'] * threshold # use rank instead

            #recession_df = df[bool1&bool2] 
           # recession_df = df[bool1] # because PET is not representative of ET, we mainly use PPT data to identify recession of Q

            #if len(recession_df)>=0.05*len(df):
             #   print(f'The recession data is {len(recession_df)/len(df)*100}% of the whole data with a threshold of {threshold}')
              #  return recession_df

        #else:
         #   print(f'The amount of recesssion data is {len(recession_df)}, which is only {len(recession_df)/len(df)*100}% of the whole data with a threshold of {threshold}')
          #  return recession_df


    def get_recession_df(self, df, recession_data_n):
        '''
        use ranking of ratios to get recession_df
        '''
        recession_df = copy.deepcopy(df)
        recession_df['Q/P'] = recession_df['QObs(mm/d)']/(recession_df['PRCP(mm/day)'] + 1e-8) # avoid zero division
        recession_df['Q/E'] = recession_df['QObs(mm/d)']/recession_df['PET(mm/day)']
        
        recession_df['Q/P_rank'] = recession_df['Q/P'].rank(pct=True)
        recession_df['Q/E_rank'] = recession_df['Q/E'].rank(pct=True)
        
        recession_df['rank_sum'] = recession_df['Q/P_rank'] + recession_df['Q/E_rank']  

        # require dQ<0
        recession_df = copy.deepcopy(recession_df[recession_df['dQ(mm/d)']<0])

        recession_df = copy.deepcopy(recession_df.sort_values(by='rank_sum',ascending=False)[:recession_data_n])
        recession_df = recession_df.loc[:, ['PRCP(mm/day)','PET(mm/day)','QObs(mm/d)','dQ(mm/d)','Qavg(mm/d)','gQ','lnQavg']]

        #print(f'The amount of recesssion data is {len(recession_df)}')
        #print('The recession data look like', recession_df.head())

        return recession_df


    def set_up_parameter_range(self):
        c1_lst, c2_lst, c3_lst = [], [], []

        # suppose 1% - 10% of training data is recession data
        # randomly draw data from recession_df with replacement for 1000 times
        # recrod c1, c2, c3 values for each time
        #print(int(0.01*len(self.train_df)), int(0.1*len(self.train_df)))

        recession_data_n_range = range(int(0.01*len(self.train_df)), int(0.1*len(self.train_df)))

        #print('recession_data_n_range', recession_data_n_range[0], recession_data_n_range[-1])
        for recession_data_n in recession_data_n_range:
            recession_df = self.get_recession_df(self.train_df, recession_data_n)
            c1, c2, c3 = self.estimate_gQ(recession_df)
            c1_lst.append(c1)
            c2_lst.append(c2)
            c3_lst.append(c3)
     
        self.parameter_range['c1'] = [np.mean(c1_lst) - 3*np.std(c1_lst), np.mean(c1_lst) + 3*np.std(c1_lst)]
        self.parameter_range['c2'] = [np.mean(c2_lst) - 3*np.std(c2_lst), np.mean(c2_lst) + 3*np.std(c2_lst)] 
        self.parameter_range['c3'] = [np.mean(c3_lst) - 3*np.std(c3_lst), np.mean(c3_lst) + 3*np.std(c3_lst)]  

    
        
    def estimate_gQ(self, df):
        """
        Regress for ln(gQ) = c1 + c2lnQ + c3lnQ2

        Inputs
        -------
        df: recession dataframe

        Returns
        -------
        Return the three coefficients corresponding confidence intervals, optimal values will be calibrated later together with PPT and PET data

        """
        y = np.log(df['gQ'].values)
        x = df['lnQavg'].values  
        X = np.array([x, x**2]).T
        X = sm.add_constant(X)

        model = sm.OLS(y, X).fit()
        c1, c2, c3 = model.params 
        return c1, c2, c3 
        #c1_range, c2_range, c3_range = model.conf_int(alpha=0.05)

       # return c1_range, c2_range, c3_range


    def calculate_gQ(self, Q, c1, c2, c3):
        if Q<=0:
            Q = 1e-8 # avoid zero Q values
        
        lnQ = np.log(Q)
        lngQ = c1 + c2*lnQ + c3*lnQ*lnQ
        gQ = np.exp(lngQ) 

        return gQ


############################ code for bayesian optimization ###########################
    def bayesian_optimization(self):
        """
        inputs
        -------
        init_sample_n: initial sample n for the gaussian model

        """
        print('\nStart bayesian optimization...')

        print(f'Get {self.init_sample_n} sample to initiate the model')
        for _ in range(self.init_sample_n):
            if self.is_converge():
                return 
            data_point = self.get_random_data_point()
            self.res.append(self.update_datapoint_with_score(data_point))
        

        #print('The number of total searched data point to start with is', len(self.res))
        # use self.res to update the gaussian model
        self.update_GPR()
        print (f"Finish GPR initialization,  {(time.time()-self.t1)/60:.2f} minutes used")
        self.save_results()

        # the convergence condition is when the NSE error is smaller than 0.01
        print('Start searching convergence')
        while not self.is_converge():
            X_sample = []  
            for _ in range(self.sample_n_acq):
                data_point = self.get_random_data_point()
                X_sample.append([data_point['c1'], data_point['c2'], data_point['c3'], data_point['runoff_coef']])

            #print('X_sample', X_sample)
            scores = self.GPRmodel.predict(X_sample, return_std=False)
            #print('scores', scores)
            
            index_from_large_to_small_score = np.argsort(scores.reshape(-1))[::-1]
            #print('index_from_large_to_small_score', index_from_large_to_small_score)
            
            for curr_index in index_from_large_to_small_score[:self.sample_n_score_calculated]:
                curr_dp = {}
                for i, par_name in enumerate(['c1', 'c2', 'c3', 'runoff_coef']):
                    curr_dp[par_name] = X_sample[curr_index][i]
                curr_dp = self.update_datapoint_with_score(curr_dp)  

                self.res.append(curr_dp)
                
                if len(self.res)>=self.opt_iter_n:
                    return

                if len(self.res)%50 == 0:
                    print('Predicted NNSE for current data is', round(scores[curr_index], 4), '. Actual NNSE for current data is', round(curr_dp['NNSE'], 4) )    
                    self.save_results()
            
            self.update_GPR() # update for every round of searching  

            # update parameter range
            if len(self.res)>5000 and len(self.res)%1000==0:
                self.update_parameter_range(print_log=False)
                print('Update parameter range:')
                print('Former range is', self.parameter_range)
                print('New range is', self.parameter_range)

        self.save_results()
        print('Finish bayesian optimization.') 


    def set_up_optimal_par(self):
        print('Set up the optimal parameters...')
        self.res_df = pd.DataFrame(self.res).sort_values('NNSE', ascending=False)

        self.c1 = self.res_df.iloc[0,:]['c1']
        self.c2 = self.res_df.iloc[0,:]['c2']
        self.c3 = self.res_df.iloc[0,:]['c3']
        self.kP = self.res_df.iloc[0,:]['kP']
        self.kE = self.res_df.iloc[0,:]['kE']


    def get_random_data_point(self):
        data_point = {}
        for par_name in self.parameter_range.keys():
            r = random.random()
            par_range_lower, par_range_upper = self.parameter_range[par_name]
            par_value = r * par_range_lower + (1-r) * par_range_upper
            data_point[par_name] = par_value
        return data_point


    def update_GPR(self):
        # create a dataframe using current results
        df = pd.DataFrame(self.res)
        X = df.loc[:,['c1', 'c2', 'c3', 'runoff_coef']].values
        y = df.loc[:,['NNSE']].values
        
        self.GPRmodel.fit(X, y)

        X = y = [] # release memory
        df = pd.DataFrame() # release memory


    def save_results(self):
        pd.DataFrame(self.res).sort_values('NNSE', ascending=False).to_csv(self.save_path+'/'+self.par_file_name)
        print(f'Results saved. Number of total visited points = {len(self.res)},{(time.time()-self.t1)/60:.2f} minutes used\n')


    def is_converge(self):
        if len(self.res) > (self.opt_iter_n): 
            print('Reach maximum iterations! Stop optimization.')
            return True

        for par_name in self.parameter_range.keys():
            lower, upper = self.parameter_range[par_name] 
            if upper-lower>0.01: # if all the upper bound and lower bounds have difference <0.01, we consider convergence
                return False
        print('Parameter range convergence detected! Stop optimization.')

        return True


    def update_parameter_range(self, print_log = False):
        if len(self.res)>=5000:
            # use the top 1000 data points to refine the range
            self.res_df = pd.DataFrame(self.res).sort_values('NNSE', ascending=False)

            for par_name in self.parameter_range.keys():
                # adjust the parameter range
                mean, sigma = np.mean(self.res_df[par_name].values[:1000]), np.std(self.res_df[par_name].values[:1000])
                new_lower, new_upper = max(mean-2*sigma,self.parameter_range[par_name][0]), min(mean+2*sigma,self.parameter_range[par_name][1])
                self.parameter_range[par_name] = new_lower, new_upper
                if print_log:
                    print(f'new range for {par_name} is {self.parameter_range[par_name]}')
        else:
            print('The record number is less than 5000, the parameter range is not updated')



    def update_datapoint_with_score(self, data_point):
        # the kP and kE parameters can be calculated by the runoff_coef
        ET_P_ratio = 1 - data_point['runoff_coef']
        data_point['kP'] = self.train_df['QObs(mm/d)'].sum()/self.train_df['PRCP(mm/day)'].sum()/data_point['runoff_coef']
        data_point['kE'] = ET_P_ratio * data_point['kP'] * self.train_df['PRCP(mm/day)'].sum() / self.train_df['PET(mm/day)'].sum()
 
        Q_predictions = self.integrate_Q(self.train_df, 
                                         kP = data_point['kP'], kE = data_point['kE'], 
                                         c1 = data_point['c1'], c2 = data_point['c2'], c3 = data_point['c3'])
        
        data_point['NSE'] = self.calculate_NSE(pred = Q_predictions, target = self.train_df['QObs(mm/d)'].values)
        data_point['NNSE'] = 1/(2-data_point['NSE'])
     
        return data_point


############################## end for code for bayesian optimization #################################################
   
    def integrate_Q(self, df, kP=None, kE=None, c1=None, c2=None, c3=None):
        """
        Inputs
        --------
        df: a dataframe having columns of 'PRCP(mm/day)', 'PET(mm/day)' and 'QObs(mm/d)'  

        Based on kE, c1, c2, c3 inputs
        Return a sequence of integrated Q, and NSE score
        """
        #print('Start integrating discharge...')
        if kP is None: 
            print('Input values for parameters are not found, use the saved optimal parameters of the model')
            kP, kE, c1, c2, c3 = self.kP, self.kE, self.c1, self.c2, self.c3
        
        #Q_init = df['QObs(mm/d)'].values[0]
        PPT_ts = df['PRCP(mm/day)'].values[:-1]
        PET_ts = df['PET(mm/day)'].values[:-1]
        
        Q_former = df['QObs(mm/d)'].values[0] # initial Q 
        Q_predictions = [Q_former]
        
        for i in range(len(PPT_ts)): # do not predict the first day
            PPT_former = PPT_ts[i] * kP
            PET_former = PET_ts[i] * kE

            for _ in range(self.timestep): # integrate with defined timestep
                if Q_former <= 1e-3: #  to avoid overflow in division
                    Q_former = 1e-3

                elif np.isinf(Q_former) or Q_former > self.Qmax * 100: # avoid too large a Q
                    Q_former = self.Qmax * 100

                g = self.calculate_gQ(Q_former, c1, c2, c3)

                delta = g*((PPT_former-PET_former)/Q_former-1) / self.timestep

                Q_curr = np.exp( np.log(Q_former) + delta) 

                if np.isnan(Q_curr):
                    print(f'Q is nan! former Q is {Q_former}, delta is {delta}')

                Q_former = Q_curr
            
            if np.isinf(Q_curr) or Q_curr > self.Qmax * 100: # avoid too large a Q
                Q_curr = self.Qmax * 100

            Q_predictions.append(Q_curr)

        return Q_predictions 



################################# unused ###################################
    def plot_gQ(self, Qmin, Qmax, c1, c2, c3, log=False, save=True):

        fig = plt.figure(figsize=(10,6))
        ax = fig.add_subplot(1, 1, 1)

        Q_range = np.linspace(Qmin, Qmax, 100)
        gQ_range = np.array([self.calculate_gQ(Q, c1, c2, c3) for Q in Q_range])    

        plt.plot(Q_range, gQ_range)
        plt.ylabel("g(Q)")

        plt.title('The curve of sensitivity function: g(Q) = dQ/dS\n'+f'ln(g(Q))={self.c1:.2f} + {self.c2:.2f}ln(Q) + {self.c3:.2f}ln(Q)2')
        plt.xlabel("Q (mm/d)")

        if log:
            ax.set_yscale('log')
            x.set_xscale('log')
            
        if save:
            plt.savefig(self.save_path + 'gQ_plot.jpg', dpi=100, bbox_inches='tight')
            

    def plot_Qrecess(self, df, resolution='d', order=1):
        fig = plt.figure(figsize=(10,12))
        ax = fig.add_subplot(2, 1, 1)

        plt.scatter(df['Q'].values, -df['dQ(mm/d)'].values, s=40, alpha=0.8)
        plt.ylabel(f'-dQ/dt (mm/{resolution}2)')
        plt.xlabel(f'Discharge(Q, mm/{resolution})')
        plt.title('Recession curve')
        
        label1 = f'ln(-dQ/dt)={self.c1:.2f} + {self.c2-1:.2f}ln(Q) + {self.c3:.2f}ln(Q)2'

        ax = fig.add_subplot(2,1,2)
        ax = sns.regplot(x = np.log(df["Q"].values), y = np.log(-df["dQ"].values),
            scatter_kws={"s": 40}, order=order, ci=95, label=label1)
        
        plt.legend()
        plt.ylabel(f'ln(-dQ/dt mm/{resolution}2)')
        plt.xlabel(f'ln(Q, mm/{resolution})')
        
        fig.tight_layout()
        plt.savefig(self.save_path + 'Q_recession_plot.jpg', dpi=100, bbox_inches='tight')

    
    def calculate_NSE(self, pred, target):
        assert(len(pred)==len(target))
        return 1 - np.sum((target-pred)**2) /  np.sum((target-np.mean(target))**2)


    def calculate_MSE(self, pred, target):
        assert(len(pred)==len(target))
        return  np.mean((target-pred)**2)




    