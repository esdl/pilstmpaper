from pathlib import Path
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd
import xarray

from neuralhydrology.datasetzoo.basedataset import BaseDataset
from neuralhydrology.utils.config import Config
from neuralhydrology.datautils.QS_physical_model import *


class CHOSEN(BaseDataset):
    """Data set class for the CHOSEN data set by [#]_ and [#]_.
    
    Parameters
    ----------
    cfg : Config
        The run configuration.
    is_train : bool 
        Defines if the dataset is used for training or evaluating. If True (training), means/stds for each feature
        are computed and stored to the run directory. If one-hot encoding is used, the mapping for the one-hot encoding 
        is created and also stored to disk. If False, a `scaler` input is expected and similarly the `id_to_int` input
        if one-hot encoding is used. 
    period : {'train', 'validation', 'test'}
        Defines the period for which the data will be loaded
    basin : str, optional
        If passed, the data for only this basin will be loaded. Otherwise the basin(s) are read from the appropriate
        basin file, corresponding to the `period`.
    additional_features : List[Dict[str, pd.DataFrame]], optional
        List of dictionaries, mapping from a basin id to a pandas DataFrame. This DataFrame will be added to the data
        loaded from the dataset and all columns are available as 'dynamic_inputs', 'evolving_attributes' and
        'target_variables'
    id_to_int : Dict[str, int], optional
        If the config argument 'use_basin_id_encoding' is True in the config and period is either 'validation' or 
        'test', this input is required. It is a dictionary, mapping from basin id to an integer (the one-hot encoding).
    scaler : Dict[str, Union[pd.Series, xarray.DataArray]], optional
        If period is either 'validation' or 'test', this input is required. It contains the centering and scaling
        for each feature and is stored to the run directory during training (train_data/train_data_scaler.yml).
        
    References
    ----------
    .. [#] Zhang, L., Moges, E., Kirchner, J. W., Coda, E., Liu, T., Wymore, A. S., et al. (2021). 
    CHOSEN: A synthesis of hydrometeorological data from intensively monitored catchments and comparative analysis of hydrologic extremes. 
    Hydrological Processes, 35(11), e14429. https://doi.org/10.1002/hyp.14429
    """

    def __init__(self,
                 cfg: Config,
                 is_train: bool,
                 period: str,
                 basin: str = None,
                 additional_features: List[Dict[str, pd.DataFrame]] = [],
                 id_to_int: Dict[str, int] = {},
                 scaler: Dict[str, Union[pd.Series, xarray.DataArray]] = {}):
        super(CHOSEN, self).__init__(cfg=cfg,
                                       is_train=is_train,
                                       period=period,
                                       basin=basin,
                                       additional_features=additional_features,
                                       id_to_int=id_to_int,
                                       scaler=scaler)

    def _load_basin_data(self, basin: str) -> pd.DataFrame:
        """Load input and output data from csv files."""
        forcings_d = {'DryCreek': ['TL_Precipitation', 'TL_Evapotranspiration', 'TL_AirTemperature'],
                      'HJAndrews': ['PRIMET_Precipitation', 'PRIMET_Evapotranspiration', 'PRIMET_AirTemperature'],
                      'HarvardForest': ['HF001_Precipitation','HF001_Evapotranspiration', 'HF001_AirTemperature'],
                      'HubbardBrook': ['WS7_Precipitation', 'STAHQ_Evapotranspiration','STAHQ_AirTemperature'],
                      'JornadaBasin': ['JWSTA_Precipitation','JWSTA_Evapotranspiration','JWSTA_AirTemperature'],
                      'Kellogg': ['KBS002_Precipitation',  'KBS002_Evapotranspiration', 'KBS002_AirTemperature'],
                      'KonzaPrairie': ['HQ01_Precipitation', 'HQ01_Evapotranspiration','HQ01_AirTemperature'],
                      'Sevilleta': ['Station44_Precipitation', 'Station44_Evapotranspiration','Station44_AirTemperature']}

        discharge_d = { 'DryCreek': ['LG_Discharge'],
                        'HJAndrews': ['GSLOOK_Discharge'],
                        'HarvardForest': ['BigelowLower_Discharge'],
                        'HubbardBrook': ['WS7_Discharge'],
                        'JornadaBasin': ['SaltCreek_Discharge'],
                        'Kellogg': ['KBS096_Discharge'],
                        'KonzaPrairie':['KingsCreek_Discharge'],
                        'Sevilleta':['Bernado_Discharge']}

        # get forcings
        #print('basin name is', basin)
        df = pd.read_csv(str(self.cfg.data_dir) + f'/{basin}_12yrs_ET.csv', index_col = 'DateTime', parse_dates=True)
        #print(df.head())
        df = df.loc[:, forcings_d[basin]+discharge_d[basin]]

        # rename columns
        new_col_names = []
        for col in df.columns:
            if 'precipitation' in col.lower():
                new_col_names.append('PRCP(mm/day)')
            elif 'evapotranspiration' in col.lower():
                new_col_names.append('PET(mm/day)')
            elif 'airtemperature' in col.lower():
                new_col_names.append('Tavg(C)')
            elif 'discharge' in col.lower():
                new_col_names.append('QObs(mm/d)')
            #elif 'solarradiation' in col.lower()
             #   new_col_names.append('SRAD(W/m2)')
            else:
                raise RuntimeError(f"Forcing type is not recognized")

         #df['PET(mm/day)'] = get_priestley_taylor_pet(t_min = df['Tavg(C)'].values,
          #                                      t_max = df['Tavg(C)'].values,
           #                                     s_rad = df['SRAD(W/m2)'].values,
            #                                    lat = att_df.loc[basin, 'gauge_lat'], 
             #                                   elev = att_df.loc[basin, 'elev_mean'],
              #                                  doy = df.index.dayofyear.values)

        df.columns = new_col_names

        ############ initialize the physical model for pilstm ##########
        if not isinstance(self.cfg.train_start_date, list):
            train_start_date = [self.cfg.train_start_date]
        else:
            train_start_date = self.cfg.train_start_date

        if not isinstance(self.cfg.train_end_date, list):
            train_end_date = [self.cfg.train_end_date]
        else:
            train_end_date = self.cfg.train_end_date


        if self.cfg.model.lower() == 'pilstm':
            physical_model = QS_physical_model(basin = basin,
                                                catchment_data = df,
                                                train_start_dates = train_start_date,
                                                train_end_dates = train_end_date,
                                                 timestep = 24,
                                                 save_path = str(self.cfg.run_dir).rsplit('/',3)[0] + '/calibration_results', #+ str(self.cfg.scenario),
                                                 init_sample_n = 500,
                                                 opt_iter_n = 15000,
                                                 sample_n_acq = 5000,
                                                 sample_n_score_calculated = 10
                                                 )
            df['QPhy(mm/d)'] = physical_model.integrate_Q(df)
            df['Qpilstm(mm/d)'] = (1-self.cfg.lambda_pilstm)*df['QObs(mm/d)'] + self.cfg.lambda_pilstm*df['QPhy(mm/d)'] 

        ##################################################################
        
        df = df.rename_axis('date')
        #print(df.head())

        return df


    #def _load_attributes(self) -> pd.DataFrame:
     #   return load_camels_us_attributes(self.cfg.data_dir, basins=self.basins)
