# Imports
from pathlib import Path
import pandas as pd
import torch
from neuralhydrology.nh_run import start_run, eval_run, finetune


# by default we assume that you have at least one CUDA-capable NVIDIA GPU
#if torch.cuda.is_available():
 #   start_run(config_file=Path("50_basins.yml"))

# fall back to CPU-only mode
#else:
start_run(config_file=Path("50_basins.yml"), gpu=-1)
